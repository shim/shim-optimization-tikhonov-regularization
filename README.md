# Shim Optimization Tikhonov Regularization

Code to accompany Shi, Clare, Vannesjo (2021) _Magnetic Resonance In Medicine_

The code will be uploaded here soon! 

Before it is, please email

Johanna Vannesjo <johanna.vannesjo@ntnu.no> or
Stuart Clare <stuart.clare@ndcn.ox.ac.uk>

to request access.
